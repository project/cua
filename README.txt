Composer update assistent

This module generates the composer commands to update modules to the recommended version. Which then can be copied and executed as a shell command:

navigate to one of the following paths:
/admin/reports/updates/update
/admin/modules/update